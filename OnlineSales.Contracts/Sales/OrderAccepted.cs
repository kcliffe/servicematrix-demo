﻿using System;

namespace OnlineSales.Contracts.Sales
{
    public class OrderAccepted
    {
        public string OrderId { get; set; }
    }
}
