using System;
using NServiceBus;
using OnlineSales.Contracts.Sales;
using OnlineSales.Internal.Messages.Billing;
using OnlineSales.Internal.Commands.Billing;


namespace OnlineSales.Billing
{
    public partial class OrderAcceptedHandler
    {
        partial void HandleImplementation(OrderAccepted message)
        {
            // TODO: OrderAcceptesHandler: Add code to handle the OrderAccepted message.
            Console.WriteLine("Billing received for order " + message.OrderId);

            Data.OrderId = message.OrderId;

            var submitPayment = new SubmitPayment() { OrderId = message.OrderId };
            Bus.Send(submitPayment);
        }

        partial void HandleImplementation(SubmitPaymentResponse message)
        {
            Console.WriteLine("Payment reponse received for order " + message.OrderId);
        }

        partial void AllMessagesReceived()
        {
            Console.WriteLine("Processing complete for order " + Data.OrderId);
        }
    }
}
