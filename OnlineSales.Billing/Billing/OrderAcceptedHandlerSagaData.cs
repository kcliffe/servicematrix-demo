﻿using System;
using NServiceBus;
using OnlineSales.Internal.Commands.Billing;
using OnlineSales.Contracts.Sales;
using OnlineSales.Internal.Messages.Billing;
using NServiceBus.Saga;


namespace OnlineSales.Billing
{
    public partial class OrderAcceptedHandlerSagaData
    {
        public string OrderId { get; set; }
    }
}
