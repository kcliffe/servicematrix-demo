﻿using System;

namespace OnlineSales.Internal.Commands.Sales
{
    public class SubmitOrder
    {
        public decimal TotalCost { get; set; }
        public long CustomerId { get; set; }
    }
}
