﻿using System;

namespace OnlineSales.Internal.Commands.Billing
{
    public class SubmitPayment
    {
        public string OrderId { get; set; }
    }
}
