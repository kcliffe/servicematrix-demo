﻿using System;

namespace OnlineSales.Internal.Messages.Billing
{
    public class SubmitPaymentResponse
    {
        public string OrderId { get; set; }

        public string AuthCode { get; set; }
    }
}
