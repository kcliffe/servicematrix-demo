﻿using System;
using NServiceBus;
using OnlineSales.Internal.Commands.Sales;
using OnlineSales.Contracts.Sales;


namespace OnlineSales.Sales
{
    public partial class SubmitOrderHandler
    {
        partial void HandleImplementation(SubmitOrder message)
        {
            var orderId = DateTime.Now.Minute.ToString();

            // TODO: SubmitOrderHandler: Add code to handle the SubmitOrder message.
            Console.WriteLine("Submit order received, allocating Id " + orderId);

            var orderAccepted = new OrderAccepted() { OrderId = orderId };
            Bus.Publish(orderAccepted);
        }
    }
}
