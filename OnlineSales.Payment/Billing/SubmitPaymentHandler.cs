﻿using System;
using NServiceBus;
using OnlineSales.Internal.Commands.Billing;
using OnlineSales.Internal.Messages.Billing;

namespace OnlineSales.Billing
{
    public partial class SubmitPaymentHandler
    {
        partial void HandleImplementation(SubmitPayment message)
        {
            // TODO: SubmitPaymentHandler: Add code to handle the SubmitPayment message.
            Console.WriteLine("Payment request received for order " + message.OrderId);

            var response = new SubmitPaymentResponse();
            Bus.Reply(response);
        }
    }
}
